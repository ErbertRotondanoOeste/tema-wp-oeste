<?php 

  if(is_maintenance_on()){
    get_template_part('maintenance');
  } else {
  
  get_header(); ?>
  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex flex-column justify-content-center align-items-center">
    <div class="container text-center text-md-left" data-aos="fade-up">
      <h1>Nós temos a <span>solução</span> para o seu negócio</h1>
      <a href="#contact" class="btn-get-started scrollto">Entre em Contato</a>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about pt-5 section-bg">
      <div class="container">

        <div class="row">
          <div class="col-lg-6">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/about.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <h3 style>Quem somos</h3>
            <?php
               $args = array(
                'name' => 'quem-somos'
              );
              // The Query
              $the_query = new WP_Query( $args );
               
              // The Loop
              if ( $the_query->have_posts() ) {
                  
                  while ( $the_query->have_posts() ) {
                      $the_query->the_post();
                      the_content();
                  }
                  
              } else {
                  // no posts found
              }
              /* Restore original Post Data */
              wp_reset_postdata(); 

              ?>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services ">
      <div class="container">
        <div class="section-title">
          <h2>Marcas</h2>
          <?php if (get_option('partners_text')) {
            ?> 
              <p class="text-secondary"> <?php echo get_option('partners_text'); ?>
            <?php } ?>
        </div>
      </div>
        <div class="row mx-3">
          <div class="brand-carousel owl-carousel">
             <?php
         $args = array(
          'post_type' => 'partners'
        );
        // The Query
        $the_query = new WP_Query( $args );
         
        // The Loop
        if ( $the_query->have_posts() ) {
            
            while ( $the_query->have_posts() ) {
                $the_query->the_post(); ?>
          
          <div class="rounded px-5">
            <div class="single-logo center-center align-middle">
                <img src="<?php the_post_thumbnail_url(); ?>" alt="">
              </div>
            
          </div>
              

           <?php
                
            }
            
        } else {
            // no posts found
        }
        /* Restore original Post Data */
        wp_reset_postdata(); 

        ?>
          </div>


      </div>
    </section><!-- End Services Section -->

    <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg">
      <div class="container">

        <div class="section-title">
          <h2>Nossas Filiais</h2>
          <?php if (get_option('branches_text')) {
            ?> 
              <p class="text-secondary"> <?php echo get_option('branches_text'); ?>
            <?php } ?>
        </div>
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
 
        <div class="row d-flex justify-content-center">
          <?php
         $args = array(
          'post_type' => 'branches'
        );
        // The Query
        $the_query = new WP_Query( $args );
         
        // The Loop
        if ( $the_query->have_posts() ) {
            
            while ( $the_query->have_posts() ) {
                $the_query->the_post();  ?>
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">      
            <div class="member">
              <img src="<?php the_post_thumbnail_url(); ?>" alt="">
              <h4><?php the_title(); ?></h4>
              <span><?php echo get_field('sigla') ?></span>
              <p>
                <?php the_content(); ?>
              </p>
              <div class="social">
                <?php if(get_field('telefone')){ ?>
                  <p> Telefone: 
                    <a href="tel:<?php echo get_field('telefone') ?>">
                      <i class="icofont-phone"></i><?php echo get_field('telefone') ?>
                    </a>
                  </p>

                <?php } ?>
                <?php if(get_field('endereco')){ ?>
                  <p> Endereço: 

                      <i class="icofont-location-pin"></i><?php echo get_field('endereco') ?>

                  </p>

                <?php } ?>
              </div>
            </div>
          </div>

        <?php        
            }
            
        } else {
            // no posts found
        }
        /* Restore original Post Data */
        wp_reset_postdata(); 

        ?>
          

        </div>

      </div>
    </section><!-- End Team Section -->

    <?php
    get_footer(); 
  }

    ?>