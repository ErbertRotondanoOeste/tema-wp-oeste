<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Oeste Tecnologia</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png" rel="icon">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
  <?php wp_head(); ?>
</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center">

      <div class="logo mr-auto">
        <h1><a href="index.html"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-oeste.png"></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav class="nav-menu d-none d-lg-block">
        
        </ul>
      </nav><!-- .nav-menu -->

      <div class="header-social-links">
        <?php if(get_option('twitter_url')){?>
          <a href="<?php echo get_option('twitter_url'); ?>" class="twitter"><i class="icofont-twitter"></i></a>
        <?php } ?>
        <?php if(get_option('facebook_url')){?>
          <a href="<?php echo get_option('facebook_url'); ?>" class="facebook"><i class="icofont-facebook"></i></a>
        <?php } ?>
        <?php if(get_option('instagram_url')){?>
        <a href="<?php echo get_option('instagram_url'); ?>" class="instagram"><i class="icofont-instagram"></i></a>
        <?php } ?>
        <?php if(get_option('linkedin_url')){?>
        <a href="<?php echo get_option('linkedin_url'); ?>" class="linkedin"><i class="icofont-linkedin"></i></i></a>
        <?php } ?>
      </div>

    </div>
  </header><!-- End Header -->
<section class="d-flex flex-column justify-content-center">
  <div class="content mt-5">
      <div class="container-fluid">
        <div class="row justify-content-center ">
          <div class="col-lg-8">
            <div class="card border-white">
              <div class="card-header text-center bg-white"><img src="https://yata-apix-61589086-5007-4240-9eec-744605e3805f.lss.locawebcorp.com.br/7d4eff6626864e6ab5be2ae448c898df.png" width="200"></div>
              
              <div class="card-body">
                <div class="text-center bg-alert">
                  <h4 class="text-secondary mb-3 mt-3">Seja bem vindo!</h4>
                  <div class="row justify-content-center pb-3">
                    <span class="text-secondary"> No momento o nosso site está em manutenção, mas volte novamente para nos visitar!</span>
                  </div>
                  <?php if (get_option('phone_number')){ ?>
                    <div class="row justify-content-center pb-3">
                      <span class="text-secondary"> Enquanto isso, que tal entrar em contato conosco, nossos atendentes esclarecerão qualquer dúvida...</span>
                    </div>
                    <style type="text/css">
                      .btn-primary{
                        background-color:#ed8600;
                        border-color: #ed8600;
                      }
                      .btn-primary:hover{
                        background-color: #ff9204;
                        border-color: #ff9204;
                      }
                    </style>
                    <div class="row justify-content-center pb-3">
                      <a class="btn btn-primary btn-lg" href="tel:<?php echo get_option('phone_number') ?>">
                          <div class="social">  
                              <i class="icofont-phone"></i><?php echo get_option('phone_number') ?>
                          </div>
                      </a>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-8 -->
          </div>


      </div><!-- /.container-fluid -->
    </div>
  </section>
  <!-- ======= Footer ======= -->
  <footer id="footer" class="fixed-bottom">
    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          <strong><?php echo date('Y') ?></strong> <span style="color:#ed8600">&copy;</span> <strong><span>Oeste Tecnologia</span></strong>
        </div>
        <div class="credits">
        </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <?php if(get_option('twitter_url')){?>
          <a href="<?php echo get_option('twitter_url'); ?>" class="twitter"><i class="bx bxl-twitter"></i></a>
        <?php } ?>
        <?php if(get_option('facebook_url')){?>
          <a href="<?php echo get_option('facebook_url'); ?>" class="facebook"><i class="bx bxl-facebook"></i></a>
        <?php } ?>
        <?php if(get_option('instagram_url')){?>
        <a href="<?php echo get_option('instagram_url'); ?>" class="instagram"><i class="bx bxl-instagram"></i></a>
        <?php } ?>
        <?php if(get_option('linkedin_url')){?>
        <a href="<?php echo get_option('linkedin_url'); ?>" class="linkedin"><i class="bx bxl-linkedin"></i></i></a>
        <?php } ?>

      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/counterup/counterup.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/venobox/venobox.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/owl.carousel/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>
  <?php wp_footer(); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/custom.js"></script>

</body>

</html>
