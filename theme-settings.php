<?php 
// ----------------------------------------------
// Elementos de Social
// ----------------------------------------------
function display_twitter_element()
{
	?>
    	<input type="text" name="twitter_url" id="twitter_url" value="<?php echo get_option('twitter_url'); ?>" />
    <?php
}

function display_facebook_element()
{
	?>
    	<input type="text" name="facebook_url" id="facebook_url" value="<?php echo get_option('facebook_url'); ?>" />
    <?php
}
function display_instagram_element()
{
    ?>
        <input type="text" name="instagram_url" id="instagram_url" value="<?php echo get_option('instagram_url'); ?>" />
    <?php
}
function display_linkedin_element()
{
    ?>
        <input type="text" name="linkedin_url" id="linkedin_url" value="<?php echo get_option('linkedin_url'); ?>" />
    <?php
}
// ----------------------------------------------
// Elementos de Contato
// ----------------------------------------------
function display_phone_element()
{
    ?>
        <input type="text" name="phone_number" id="phone_number" value="<?php echo get_option('phone_number'); ?>" />
    <?php
}
function display_email_element()
{
    ?>
        <input type="mail" name="email" id="email" value="<?php echo get_option('email'); ?>" />
    <?php
}
function display_address_element(){
 ?>
        <input type="text" name="address" id="address" rows="4" cols="50" value="<?php echo get_option('address'); ?>" />
    <?php   
}

// ----------------------------------------------
// Elementos de Texto da home
// ----------------------------------------------

function display_partners_text_element()
{
    ?>
        <input type="text" name="partners_text" id="partners_text" value="<?php echo get_option('partners_text'); ?>" />
    <?php
}
function display_testimonials_text_element()
{
    ?>
        <input type="text" name="testimonials_text" id="testimonials_text" rows="4" cols="50" value="<?php echo get_option('testimonials_text'); ?>" />
    <?php
}
function display_branches_text_element()
{
    ?>
        <input type="text" name="branches_text" id="branches_text" rows="4" cols="50" value="<?php echo get_option('branches_text'); ?>" />
    <?php
}
function display_contact_text_element()
{
    ?>
        <input type="text" name="contact_text" id="contact_text" rows="4" cols="50" value="<?php echo get_option('contact_text'); ?>" />
    <?php
}
// ----------------------------------------------
// Elementos de Manutenção
// ----------------------------------------------
function display_maintenance_toggle_element(){
 ?>  
          <select name="maintenance_on">
            <option value='desligado'
              <?php if(get_option('maintenance_on') == 'desligado') { 
                echo "selected"; 
                } ?>
                >Desligado</option>
             <option value='ligado' 
              <?php if( get_option('maintenance_on') == 'ligado' ) { 
                echo "selected";
                } ?>
                >Ligado</option>
          </select>
    <?php   
}
// ----------------------------------------------
// Cadastro da página de theme options
// ----------------------------------------------
function display_theme_panel_fields()
{
	add_settings_section("section", "Configurações de contato", null, "theme-options");
	
	add_settings_field("twitter_url", "Endereço de perfil do Twitter:", "display_twitter_element", "theme-options", "section");
    add_settings_field("facebook_url", "Endereço de perfil do Facebook:", "display_facebook_element", "theme-options", "section");
    add_settings_field("instagram_url", "Endereço de perfil do Instagram:", "display_instagram_element", "theme-options", "section");
    add_settings_field("linkedin_url", "Endereço de perfil do Linkedin:", "display_linkedin_element", "theme-options", "section");
    add_settings_field("phone_number", "Telefone para contato:", "display_phone_element", "theme-options", "section");
    add_settings_field("email", "Email:", "display_email_element", "theme-options", "section");
    add_settings_field("address", "Endereço:", "display_address_element", "theme-options", "section");
    add_settings_field("maintenance_on", "Modo de Manutenção:", "display_maintenance_toggle_element", "theme-options", "section");
    // Text fields
    add_settings_field("partners_text", "Texto da área de Marcas", "display_partners_text_element", "theme-options", "section");
    add_settings_field("testimonials_text", "Texto da área de Testemunhos", "display_testimonials_text_element", "theme-options", "section");
    add_settings_field("branches_text", "Texto da área de Filiais", "display_branches_text_element", "theme-options", "section");
    add_settings_field("contact_text", "Texto da área de Contato", "display_contact_text_element", "theme-options", "section");

    register_setting("section", "twitter_url");
    register_setting("section", "facebook_url");
    register_setting("section", "instagram_url");
    register_setting("section", "linkedin_url");
    register_setting("section", "phone_number");
    register_setting("section", "email");
    register_setting("section", "address");
    register_setting("section", "maintenance_on");

    register_setting("section", "partners_text");
    register_setting("section", "testimonials_text");
    register_setting("section", "branches_text");
    register_setting("section", "contact_text");    
}

add_action("admin_init", "display_theme_panel_fields");

function theme_settings_page(){
    ?>
    <div class="wrap">
        <h1>Configurações de Tema</h1>
        <form method="post" action="options.php">
            <?php
                settings_fields("section");
                do_settings_sections("theme-options");      
                submit_button(); 
            ?>          
        </form>
    </div>
    <?php
}

function add_theme_menu_item()
{
    add_menu_page("Configurações do Tema", "Configurações do Tema", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");
// ----------------------------------------------
// 
// ----------------------------------------------
?>