<?php 

add_theme_support('post-thumbnails');
add_theme_support( 'post-thumbnails', array( 'testimonials' ));

// get the the role object
$editor = get_role('editor');
// add $cap capability to this role object
$editor->add_cap('manage_options');	

include 'theme-settings.php';

function is_maintenance_on(){
	if(get_option('maintenance_on') == 'ligado'){
		return true;
	} else if (get_option('maintenance_on') == 'desligado') {
		return false;
	}
}
 ?>
