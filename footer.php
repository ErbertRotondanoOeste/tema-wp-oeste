<!-- ======= Contact Section ======= -->
    <section id="contact" class="contact section-bg">
      <div class="container">

        <div class="section-title">
          <h2>Entre em contato</h2>
          <?php if (get_option('contact_text')) {
            ?> 
              <p class="text-secondary"> <?php echo get_option('contact_text'); ?>
            <?php } ?>
        </div>

        <div class="row mt-2 justify-content-center">
          <div class="col-lg-10">
            <form class="php-email-form">
              <div class="form-row">
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Seu nome:" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                </div>
                <div class="col-md-6 form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Seu Email:" data-rule="email" data-msg="Please enter a valid email" />
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Assunto: " data-rule="minlen:4" data-msg="Por favor, insira ao menos 8 caracteres para o assunto" />
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Escreva algo..." placeholder="Conteúdo da mensagem:"></textarea>
              </div>
              <div class="text-center"><button type="submit">Enviar mensagem</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer section-bg">
    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          <strong><?php echo date('Y') ?></strong> <span style="color:#ed8600">&copy;</span> <strong><span>Oeste Tecnologia</span></strong>
        </div>
        <div class="credits">
        </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <?php if(get_option('twitter_url')){?>
          <a href="<?php echo get_option('twitter_url'); ?>" class="twitter"><i class="bx bxl-twitter"></i></a>
        <?php } ?>
        <?php if(get_option('facebook_url')){?>
          <a href="<?php echo get_option('facebook_url'); ?>" class="facebook"><i class="bx bxl-facebook"></i></a>
        <?php } ?>
        <?php if(get_option('instagram_url')){?>
        <a href="<?php echo get_option('instagram_url'); ?>" class="instagram"><i class="bx bxl-instagram"></i></a>
        <?php } ?>
        <?php if(get_option('linkedin_url')){?>
        <a href="<?php echo get_option('linkedin_url'); ?>" class="linkedin"><i class="bx bxl-linkedin"></i></a>
        <?php } ?>

      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/counterup/counterup.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/venobox/venobox.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/owl.carousel/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>
  <?php wp_footer(); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/custom.js"></script>

</body>

</html>