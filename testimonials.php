 <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials section-bg">
      <div class="container">

        <div class="section-title">
          <h2>Depoimentos</h2>
          <?php if (get_option('testimonials_text')) {
            ?> 
              <p class="text-secondary"> <?php echo get_option('testimonials_text'); ?>
            <?php } ?>
        </div>

        <div class="owl-carousel testimonials-carousel">

        <?php
         $args = array(
          'post_type' => 'testimonial',
          'posts_per_page' => 5
        );
        // The Query
        $the_query = new WP_Query( $args );
         
        // The Loop
        if ( $the_query->have_posts() ) {
            
            while ( $the_query->have_posts() ) {
                $the_query->the_post(); ?>

                <div class="testimonial-item pb-5">
                  <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    <?php the_content(); ?>
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                  </p>
                  <img src="<?php the_post_thumbnail_url(); ?>" class="testimonial-img" alt="">
                  <h3><?php echo get_field('parceiro') ?></h3>
                  <h4><?php echo get_field('cargo_do_parceiro') ?></h4>
                </div>

                <?php
                
            }
            
        } else {
            // no posts found
        }
        /* Restore original Post Data */
        wp_reset_postdata(); 

        ?>
          


        </div>

      </div>
    </section><!-- End Testimonials Section -->